public with sharing class ReportExportController {
    
    public Id reportId {get; set;} // No longer used but here for backwards compatability
    public String reportFormat {get; set;} //At the moment only CSV allowed
    public List<Data_Consumer_Request_Item__c> cohorts {
        get {
            if(cohorts == null) {
                cohorts = new List<Data_Consumer_Request_Item__c>();
                try {
                    cohorts = Database.query(soql);
                } catch(Exception ex){}
            }
            System.debug(loggingLevel.DEBUG,'ReportExportController.cls: Line 13 - ==>' + JSON.serialize(cohorts));
            return cohorts;
        } set;
    }

    public String soql {
        get {
            if(soql == null) {
                soql = '';
                for(Schema.FieldSetMember field : getCSVExportFields()) {
                    soql += (soql == '')?field.getFieldPath():',' + field.getFieldPath();
                }
                soql = 'Select ' + soql + ' from Data_Consumer_Request_Item__c where LastModifiedDate = TODAY and Domains_or_Content_Areas__c != null';
            }
            return soql;
        } set;
    }

    public String output {
        get {
            //Get the headings
            String headings = '';
            for(Schema.FieldSetMember field : getCSVExportFields()) {
                headings += (headings == '')?field.getLabel():',' + field.getLabel();
            }
             String dataRows = '';
            if(headings != '') {
                //Add a new line
                headings += '\r\n';
                //Get the data rows
                if(!cohorts.isEmpty()) {
                    for(Data_Consumer_Request_Item__c cohort : cohorts) {
                        String dataRow = '';
                        for(Schema.FieldSetMember field : getCSVExportFields()) {
                            String value = '';
                            if(field.getFieldPath().contains('.')) {
                                String parentObj = field.getFieldPath().split('\\.')[0];
                                String parentField = field.getFieldPath().split('\\.')[1];
                                value = (string)cohort.getSObject(parentObj).get(parentField);
                            } else {
                                value = (string)cohort.get(field.getFieldPath());
                            }
                            if(field.getType() == Schema.DisplayType.DateTime) {
                                value = DateTime.valueOf(value).format();
                            }
                            if(field.getType() == Schema.DisplayType.Date) {
                                value = Date.valueOf(value).format();
                            }
                            dataRow += (dataRow == '')?value:',' + value;
                        }
                        dataRows += (dataRows == '')?dataRow:'\r\n' + dataRow;
                    }
                }
            }
            dataRows = dataRows.replace('null', '');
            return headings + dataRows;
        } set;
    }
    
    public List<Schema.FieldSetMember> getCSVExportFields() {
        return SObjectType.Data_Consumer_Request_Item__c.FieldSets.Applications_with_Cohorts_Export_Fields.getFields();
    }
    
    /*
    public Id reportId {get; set;}
    public String reportFormat {get; set;} //At the moment only CSV allowed

    public String output {
        get {
            // Run a report synchronously
            Reports.reportResults results = Reports.ReportManager.runReport(reportId, true);

            // Get the headings
            String headings = '';
            Reports.ReportExtendedMetadata rmd =   results.getReportExtendedMetadata();
            Map<String,Reports.DetailColumn> colMap = rmd.getDetailColumnInfo();
            for(String key : colMap.KeySet()){
                headings += (headings == '')?colMap.get(key).getLabel():',' + colMap.get(key).getLabel();
            }
            String dataRows = '';
            if(headings != '') {
                //Add a new line
                headings += '\r\n';
                //Get the data rows
                Reports.ReportFactWithDetails factDetails = (Reports.ReportFactWithDetails)results.getFactMap().get('T!T');
                List<Reports.ReportDetailRow> rows = factDetails.getRows();
                for(Reports.ReportDetailRow r : rows) {
                    String dataRow = '';
                    for(Reports.ReportDataCell c : r.getDataCells()) {
                        dataRow += (dataRow == '')?c.getLabel():',' + c.getLabel();       
                    }
                    dataRows += (dataRows == '')?dataRow:'\r\n' + dataRow;
                }
            }
            return headings + dataRows;
        }set;
    }
    */ 
}