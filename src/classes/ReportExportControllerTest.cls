@isTest //(seeAllData=true) // Needed because otherwise Report table is blank
private class ReportExportControllerTest {
    @isTest
    static void testCsvExport(){
        //List<Report> reports = [SELECT Id, Name FROM Report ORDER BY Name LIMIT 1]; // Even a fresh uncustomized org should have some sample reports but it doesn't hurt to play safe.

        //if(!reports.isEmpty()){
            ReportExportController ctrl = new ReportExportController();
            //ctrl.reportId = reports[0].Id;
            ctrl.reportFormat = 'csv';
            String sJSON = '[{"attributes":{"type":"Data_Consumer_Request_Item__c","url":"/services/data/v44.0/sobjects/Data_Consumer_Request_Item__c/a160k000001LMPDAA4"},"Application__c":"a150k000001OQFhAAO","Data_Request_Study_Salesforce_ID__c":"a160k000001LMPDAA4","Data_Request_Study_Update_Date_and_Time__c":"2018-11-16 00:30:16","Data_Request_Study_Level_2_Status__c":"IE - Enquiry Received","Name":"c1","Domains_or_Content_Areas__c":"Allergic reaction to substance;Identifiers and miscellaneous","Feature_Measures__c":"ECRHS III: Third European Community Respiratory Health Survey derived","Id":"a160k000001LMPDAA4","Application__r":{"attributes":{"type":"Data_Consumer_Request__c","url":"/services/data/v44.0/sobjects/Data_Consumer_Request__c/a150k000001OQFhAAO"},"Id":"a150k000001OQFhAAO","Data_Consumer_Salesforce_ID__c":"0030k00000kaq7DAAQ","Data_Consumer_First_Name__c":"Dave","Data_Consumer_Last_Name__c":"Burke","Data_Request_Creation_Date_and_Time__c":"2018-11-16 00:22:52","Data_Request_Level_1_Status__c":"Initial Enquiry","Data_Request_Update_Date_and_Time__c":"2018-11-16 00:30:16"}}]';
            ctrl.cohorts = (List<Data_Consumer_Request_Item__c>)JSON.deserialize(sJSON, List<Data_Consumer_Request_Item__c>.class);
            String s = ctrl.output;
            String q = ctrl.soql;
        //}
    }
}